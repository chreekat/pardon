{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Rap where

import Control.Applicative (liftA2)
import Control.Arrow (first)
import Control.DeepSeq (NFData, deepseq, force)
import Control.Monad (guard)
import Criterion.Main (defaultMain, bench, whnf, bgroup)
import Data.Char (isSpace, isPrint)
import Data.Coerce (coerce)
import Data.Function (on, fix)
import Data.List (minimumBy, unfoldr)
import Data.List.NonEmpty (NonEmpty(..))
import GHC.Generics (Generic)
import GHC.Stack (HasCallStack)
import System.Environment (getArgs)
import Test.Tasty (TestName, TestTree, defaultMain, testGroup)
import Test.Tasty.HUnit (Assertion, (@?=), testCase)
import Test.Tasty.QuickCheck -- (Arbitrary, NonNegative(..), arbitrary, shrink, Gen, Testable, getSize, choose, resize, listOf1, scale, suchThat, frequency, getNonNegative, genericShrink, testProperty, (===), (==>))
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Vector as V

-- import Debug.Pretty.Simple
-- import Text.Pretty.Simple

data Cost = Le Int | ThatsTooMuchMan
    deriving (Eq, Show, Generic, NFData)

plus ThatsTooMuchMan _ = ThatsTooMuchMan
plus _ ThatsTooMuchMan = ThatsTooMuchMan
plus (Le a) (Le b) = Le (a + b)

costCmp ThatsTooMuchMan _ = GT
costCmp _ ThatsTooMuchMan = LT
costCmp (Le a) (Le b) = compare a b

notTooMuch a = LT == costCmp a ThatsTooMuchMan

{--

Ok, let's break this down some. I need to calculate line costs for every* line
that starts at word n and ends at word m. To do that, I need the cost of each
word, and a cost for space.

We can have a Vector Text for the words.

A Vector Int for the length of each word.

A 2-level vector of line costs. First dimension is start word, second dimension
is (mumble)


Let's just build those three things and move forward.

BUT: We can actually replace the last two things with a simple scan of word
length. Please note that this replaced a lot of code and angry comments.

--}


cost :: Int -> Int -> Cost
cost width l =
    if l > width
    then ThatsTooMuchMan
    else Le ((width - l) ^ 2)

-- | Cost of line starting at 0-index word i of length n
offsetCost width offsets i n = cost width l where
    l = n - 1 + (((-) `on` (offsets V.!)) (i + n) i)

{--

Now I have the things: V of words, V of scanlength, and a way to calculate cost.

Now I think the algo should be easier.

Uh, what's the algo again? It's a foldr I think.


        foldr f z as = a1 `f` (a2 `f` (a3 `f` z))


NOTE: We are not doing the 'ignore last line' thing yet.


So, uh, we start at the last word and find the best solution. There's only one.
I's the best. Stuff it and its cost in the <ish>.

We are at the ith word. There are length(prevSolns) possible solutions.
Each solution j has cost (offsetCost(i)(j) + solnCost (j)).

The best solution minimizes that cost.

The solution is represented by the index where the line ends + 1. Stuff the
solnCost and solution for i in the <ish>.

I could use constructrN for this, although I have to re-extract the length each
time, which is bullshit. I mean it's O(1), but still. :( Oh well, let's do it.

The set of possible solutions at step i is a zip of lineCosts ! i with the
already-constructed v. Let's build that first.

But, kinda.

[lc1]                            -->      [(lc1, N)]
[]

[lc1, lc2]                       -->  [(lc1+pc1, i+1), (lc2, N)]
[pc1]

[lc1, lc2, lc3]                  -->  [(lc1+pc1, i+1), (lc2+pc2, i+2),
[pc1, pc2, pc3, ..., pcN]              (lc3+pc3, i+3)]

While length(lineCosts ! i) > length(prevSolns), we need to add a poss solution
that starts at i and ends at N and has cost last(lineCosts ! i).

Unfortunately, cons and snoc are both O(n), which kinda blows. Is there some
other way? We're limited by constructrN, which always supplies the first empty
vector.

Well, it's just a special unfoldrN, right? Mind-bendy though.

Actually, I think we can just nudge our i and deal with it that way, such that
the lcs and pcs line up!

Update: that fixes the tail, but now I'm missing the head.
Update update: iterating once more fixes that.
--}

-- | Returns a list, so it can be lazily consumed without excess Vector
-- allocations.
possSolns :: Int -> V.Vector Int -> V.Vector (Cost, Int) -> [(Cost, Int)]
possSolns width offsets prevSolns = go where
    n = V.length offsets - 1
    r = V.length prevSolns
    i = n - r
    ocost a b = offsetCost width offsets a b
    go | V.null prevSolns
       = [(Le 0, i + 1)]
       | ocost i 1 == ThatsTooMuchMan
       = [(fst (V.head prevSolns), i + 1)]
       | otherwise
       = {-# SCC "possSolns.go.main" #-}
         takeWhile (notTooMuch . fst) (V.ifoldr buildSoln [] prevSolns)
    buildSoln j' (prevCost, prevSoln) =
        let j = j' + 1
            c = ocost i j
        in ((plus c prevCost, i + j) :)

stepSoln width offsets = minimumBy (costCmp `on` fst) . possSolns width offsets

solns width offsets ws = V.constructrN (V.length ws + 1) (stepSoln' width offsets) where
    -- deepseq previous iteration to prevent spaceleak
    stepSoln' w o p = (p V.!? 0) `deepseq` stepSoln w o p

-- | Flatten a solution
flatten
    :: V.Vector Int
    -- ^ Vector of all possible subsolutions
    -> [Int]
    -- ^ Just the final solution
flatten allSolns = unfoldr flatten 0 where
    n = V.length allSolns
    flatten i = do
        j <- allSolns V.!? i
        guard (n > j)
        pure (j,j)

soln width input = go where
    scroll = V.fromList (T.words input)
    offsets = V.scanl' (+) 0 (fmap T.length scroll)
    go = flatten (V.map snd (solns width offsets scroll))


-- | Render a solution
render :: V.Vector T.Text -> [Int] -> [T.Text]
render scroll soln = unfoldr go (0, soln) where
    go (i, []) = Nothing
    go (i, j:ss) =
        Just (T.unwords (V.toList (V.slice i (j - i) scroll)), (j, ss))

-- | (W)rap some input to lines of optimum length.
rap :: Int -> T.Text -> [T.Text]
rap width input = go where
    scroll = V.fromList (T.words input)
    offsets = V.scanl' (+) 0 (fmap T.length scroll)
    go = render scroll (flatten (V.map snd (solns width offsets scroll)))

newtype Prose = Prose T.Text
    deriving (Eq, Show)

-- Arbitrary prose has the following characteristics.
--
-- 1. It is comprised of
--     A. 0 or more lines of leading whitespace
--     B. 1 or more paragraphs, which are comprised of
--         i. 1 or more lines of printable text
--         ii. 0 or more blank footer lines
-- 2. The overall number of characters is linear w.r.t. the size parameter.
--     A. The characters are arbitrarily assigned to more, smaller paragraphs,
--        or fewer, larger paragraphs.
--     B. The size of the footer is proportional to lg sz.
-- 3. To shrink arbitrary prose, I'm just gonna remove chars from the tail of
--    the text.
instance Arbitrary Prose where
    arbitrary = do
        sz <- getSize
        blankLines <- T.replicate <$> choose (0,4) <*> pure "\n"
        parSize <- if sz > 0 then choose (1,sz) else pure 0
        let parCount = sz - parSize
        fmap (Prose . (blankLines <>) . T.concat . fmap getParagraph)
            (resize parCount (listOf1 (resize parSize arbitrary)))

    shrink = coerciveTextShrink


-- | This wasn't quite what I was looking for, but I'm keeping it for posterity.
pattern CoercivelyNull <- (T.null . coerce -> True)

coerciveTextShrink CoercivelyNull = []
coerciveTextShrink x = coerce . init . T.inits . coerce $ x

newtype Paragraph = Paragraph { getParagraph :: T.Text }
    deriving (Eq, Show, Generic)

instance Arbitrary Paragraph where
    arbitrary = coerce $ do
        foot <- T.pack <$> scale (floor . log . fromIntegral) (listOf1 (pure '\n'))
        p <- T.unlines . fmap getPrintableLine <$> listOf1 arbitrary
        pure (p <> foot)

    shrink = coerciveTextShrink

-- For now, a printable line l has |l|>0 and starts with a non-blank character.
newtype PrintableLine = PrintableLine { getPrintableLine :: T.Text }
    deriving (Eq, Show, Generic)

instance Arbitrary PrintableLine where
    arbitrary = coerce <$> l `suchThat` (not . T.null)
        where l = T.pack . dropWhile isSpace <$> listOf1 printableChar

    shrink = coerciveTextShrink

printableChar :: Gen Char
printableChar = frequency
    [(1, pure ' ')
    ,(5, arbitrary `suchThat` (\x -> isPrint x && not (isSpace x)))
    ]

-- |
-- 'Par' is some text with many trailing newlines.
--
data Par = Par T.Text Word
    deriving (Eq, Show)

instance Arbitrary Par where
    arbitrary =
        let t = T.unwords . fmap getPrintableLine <$> listOf1 arbitrary
            fot0 = scale footSz (getNonNegative <$> arbitrary)
            footSz = floor . log . fromIntegral
        in Par <$> t <*> fot0

    shrink (Par "!" 0) = []
    shrink (Par t i)
        = (Par "!" 0) : (Par <$> initail t <*> [0..i])
        where initail = tail . T.inits

-- | Pars is many blank header lines and many Pars (= lines of text plus footers).
-- 'Par's within a 'Pars' are assumed to have a tail of >2 newlines; otherwise
-- they wouldn't be Par at all. Thus we add/remove 2 to the tail of a 'Par' when
-- printing and parsing them. The last Par, however, can have a tail of 0
-- newlines, so there are no such shenanigans.
data Pars = Pars Word [Par]
    deriving (Eq, Show, Generic)

-- Need to scale the list of pars to prevent execution blowup
instance Arbitrary Pars where
    arbitrary = do
        let sqrt' = floor . sqrt . fromIntegral
        headerSz <- choose (0, 4)
        Pars headerSz <$> scale sqrt' arbitrary

    shrink x@(Pars hdr pars) = go where
        nullCase = Pars 0 []
        go | x == nullCase = []
           | otherwise = nullCase : genericShrink x

wlength :: T.Text -> Word
wlength = fromIntegral . T.length

-- | Split Text into Pars.
--
-- First remove any leading whitespace. Then
--
-- (a miracle occurs)
pars :: T.Text -> Pars
pars inputText = Pars (wlength leading) (splitPars partext) where
    (leading, partext) = T.span (== '\n') inputText

-- | Low level split. Assumes the input doesn't start with blank lines.
splitPars :: T.Text -> [Par]
splitPars = fix f where
    f next t = case T.breakOn "\n\n" t of
        -- t was null.
        ("", "") -> []
        -- There is only one paragraph. Check if there's a newline.
        (firstPar, "") -> case T.unsnoc firstPar of
            Just (strippedFirstPar, '\n') -> [Par strippedFirstPar 1]
            _ -> [Par firstPar 0]
        -- There are multiple paragraphs.
        (firstPar, x) ->
            let (wlength -> foot, rest) = T.span (== '\n') x
            in case rest of
                "" -> [Par firstPar foot]
                _ -> Par firstPar (foot - 2) : next rest

-- | Like fmap but where f "" = "".
tfmap :: (T.Text -> T.Text) -> T.Text -> T.Text
tfmap _ "" = ""
tfmap f t = f t

profMain = do
    [a,b,c] <- getArgs
    let len = read a
        exp = read c
    -- inp <- (T.unwords . take (2 * 10 ^ exp) . T.words) <$> T.readFile b
    let inp = T.unwords (replicate (10 ^ exp) "a")
    inp `deepseq` print (length (soln len inp))

main = do
    [a] <- getArgs
    let len = read a
    inp <- T.getContents
    let out = rap len inp
    T.putStr (T.unlines out)

benchMain = do
    let mkAs ct = T.unwords (replicate ct "a")
        len = 1
        counts = [100,200..1000]
        inputs = force (map mkAs counts)
        mkBench i as = bench (show i) (whnf (soln len) as)
    Criterion.Main.defaultMain
        [ bgroup ("rap " <> show len) (zipWith mkBench counts inputs) ]

shouldb :: (Eq a, Show a, HasCallStack) => a -> a -> Assertion
shouldb = (@?=)

shouldbj :: (Eq a, Show a, HasCallStack) => Maybe a -> a -> Assertion
shouldbj scrutinee standard = scrutinee @?= Just standard

it = testCase

prop :: (Testable a, HasCallStack) => TestName -> a -> TestTree
prop = testProperty

testMain = Test.Tasty.defaultMain $ do
    testGroup "rap"
        [ testGroup "pars"
            [ testGroup "base cases"
                [ it "null input" $ pars "" `shouldb` Pars 0 []
                , it "blank line" $ pars "\n" `shouldb` Pars 1 []
                , it "empty par" $ pars "\n\n" `shouldb` Pars 2 []
                , it "footed par" $
                    pars "a\n" `shouldb` Pars 0 [Par "a" 1]
                , it "two footed par" $
                    pars "a\n\n" `shouldb` Pars 0 [Par "a" 2]
                , it "blank + footless" $
                    pars "\n\na" `shouldb` Pars 2 [Par "a" 0]
                , it "simple + footless" $
                    pars "a\n\nb" `shouldb` Pars 0 [Par "a" 0, Par "b" 0]
                ]
            , testGroup "properties"
                [ prop "Maintains header"
                    $ \(Paragraph p) (NonNegative i) ->
                        let ii = T.replicate (fromIntegral i) "\n"
                            Pars j (last -> Par _ _) = pars (ii <> p)
                        in classify (i > 0) "non-zero header" (i === j)
                , prop "Maintains footer"
                    $ \(Paragraph p) (NonNegative i) ->
                        let ii = T.replicate (fromIntegral i) "\n"
                            Pars _ (last -> Par _ j) = pars (T.strip p <> ii)
                        in classify (i > 0) "non-zero footer" (i === j)
                ]
            ]
        ]
